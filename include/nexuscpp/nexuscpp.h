//*****************************************************************************
/// Synchrotron SOLEIL
///
/// NeXus C++ API
///
/// Creation : 16/02/2005
/// Authors  : Stephane Poirier, Clement Rodriguez, Nicolas Leclerc, Julien Berthaud
///
/// This program is free software; you can redistribute it and/or modify it under
/// the terms of the GNU General Public License as published by the Free Software
/// Foundation; version 2 of the License.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
/// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
///
//*****************************************************************************

#ifndef __NX_CPP_H__
#define __NX_CPP_H__

// ----------------------------------------------------------------------------
//! \mainpage NeXusCPP library
//! \tableofcontents
//! NeXusCPP is a C++ NeXus files and datasets library.\n
//! It is built on top of the HDF5 library.\n
//! Its purpose is to provide the developer with useful, easy to use and
//! powerful C++ tools for creating/reading/writing NeXus files.\n
//! The topics currently addressed by NeXusCPP library are:
//!   - \subpage lowlevelPage "Low level API"
//!   - \subpage highlevelPage "High level API"
//!   - \subpage streamingPage "Streaming API"
//!
//! <b>Main Author & Maintainer:</b>\n
//!   - Stephane Poirier - Synchrotron SOLEIL - France\n
//!
//! <b>NeXusCPP license:</b>
//!
//! Copyright (C) 2006-2021 The Tango Community (http://tango-controls.org)
//!
//! The NeXusCPP library is free software; you can redistribute it and/or modify it
//! under the terms of the GNU General Public License as published by the Free
//! Software Foundation; either version 2 of the License, or (at your option)
//! any later version.
//!
//! The NeXusCPP library is distributed in the hope that it will be useful,
//! but WITHOUT ANY WARRANTY; without even the implied warranty of
//! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
//! Public License for more details.
//!
//! See COPYING file for license details
//!
//! <b>Main Contact:</b>\n
//!   Stephane Poirier - Synchrotron SOLEIL - France
// ----------------------------------------------------------------------------

// standard library objets
#include <iostream>
#include <vector>
#include <set>

#include <nexuscpp/nxfile.h>
#include <nexuscpp/nxwriter.h>
#include <nexuscpp/nxbuffer.h>
#include <nexuscpp/nxfinalizer.h>

#endif

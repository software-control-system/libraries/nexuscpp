//*****************************************************************************
/// Synchrotron SOLEIL
///
/// NeXus C++ API
///
/// Creation : 16/02/2005
/// Authors  : Stephane Poirier, Clement Rodriguez, Nicolas Leclerc, Julien Berthaud
///
/// This program is free software; you can redistribute it and/or modify it under
/// the terms of the GNU General Public License as published by the Free Software
/// Foundation; version 2 of the License.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
/// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
///
//*****************************************************************************

#pragma once

#include <list>
#include <yat/threading/Mutex.h>
#include <yat/threading/Pulser.h>
#include <yat/any/GenericContainer.h>

namespace nxcpp
{

const std::size_t kDEFAULT_PULSER_PERIOD = 3000;

// Forward declaration
class DataStreamer;

//-----------------------------------------------------------------------------
//! NexusDataStreamerFinalizer
//-----------------------------------------------------------------------------
class NEXUSCPP_DECL NexusDataStreamerFinalizer
{
public:

  typedef struct Entry
  {
    //! The nexuscpp::DataStreamer to finalize
    nxcpp::DataStreamer *data_streamer;

    //! Opaque data holder
    //!   for data-items making use of the nexuscpp::DataStreamer::NO_COPY mode
    //!   this will be simply deleted once the data streaming is done
    //!   use this to ensure that the memory blocks containing data are not released before streaming is done
    yat::Container *data_holder;

    //! default c-tor
    Entry () : data_streamer(0), data_holder(0) {}

    //! Copy c-tot
    Entry (Entry& src) : data_streamer(src.data_streamer), data_holder(src.data_holder)
      { src.data_streamer = 0; src.data_holder = 0; }

    //! Copy operator
    void operator= (Entry& src)
      { data_streamer = src.data_streamer; src.data_streamer = 0; data_holder = src.data_holder; src.data_holder = 0; }

    //! d-tor
    ~Entry () { delete data_streamer; delete data_holder; }
  } Entry;

  //-------------------------------------------------------
  NexusDataStreamerFinalizer();

  //-------------------------------------------------------
  virtual ~NexusDataStreamerFinalizer();

  //! Start the finalyser
  //!
  //! \param period_ms finalizer period in ms
  void start(std::size_t period_ms = kDEFAULT_PULSER_PERIOD);

  //! Stop the finalyser
  void stop();

  //! Push a datastreamer entry
  void push(Entry* e);

private:
  //--------------------------------------------------------
  typedef std::list<Entry*>       Entries;
  typedef Entries::iterator       EntriesIterator;
  typedef Entries::const_iterator EntriesConstIterator;
  //--------------------------------------------------------
  void pulse(yat::Thread::IOArg arg);
  //--------------------------------------------------------
  void initialize(const std::size_t period_ms);
  void terminate();
  //--------------------------------------------------------
  inline bool initialized() const { return m_pulser != 0; }
  //--------------------------------------------------------
  yat::Pulser*  m_pulser;
  Entries       m_entries;
  yat::Mutex    m_entries_mutex;
  std::size_t   m_period_ms;
};

}

//*****************************************************************************
/// Synchrotron SOLEIL
///
/// NeXus C++ API
///
/// Creation : 16/02/2005
/// Authors  : Stephane Poirier, Clement Rodriguez, Nicolas Leclerc, Julien Berthaud
///
/// This program is free software; you can redistribute it and/or modify it under
/// the terms of the GNU General Public License as published by the Free Software
/// Foundation; version 2 of the License.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
/// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
///
//*****************************************************************************

#ifndef __NX_WRITER_H__
#define __NX_WRITER_H__

// ============================================================================
//! \page highlevelPage High level NeXus API documentation
//! \tableofcontents
//!
//! \section secB1 High level NeXus writing API
//! The high level writing API provide classes allow writing large datasets in
//! synchronous or asynchonous mode
//!
//! \section secB2 High level NeXus writing classes
//! Links to classes : \n
//!   - nxcpp::DatasetWriter
//!   - nxcpp::GenericDatasetWriter
//!   - nxcpp::AxisDatasetWriter
//!   - nxcpp::SignalDatasetWriter
//!   - nxcpp::NexusFileWriter
//!   - nxcpp::MMetadata
//!   - nxcpp::IExceptionHandler
//!   - nxcpp::IMessageHandler
//!
// ============================================================================

// standard library objets
#include <iostream>
#include <vector>
#include <set>

// yat library
#include <yat/CommonHeader.h>
#include <yat/memory/SharedPtr.h>
#include <yat/memory/MemBuf.h>
#include <yat/utils/String.h>
#include <yat/utils/Logging.h>
#include <yat/time/Timer.h>
#include <yat/threading/Mutex.h>

#include <nexuscpp/nxfile.h>

namespace nxcpp
{

// Forward declaration
class NexusFileWriterImpl;

typedef yat::SharedPtr<NexusDataSet, yat::Mutex> NexusDataSetPtr;
typedef yat::SharedPtr<NexusAttr, yat::Mutex> NexusAttrPtr;

// internal free functions
void message_to_console(yat::ELogLevel lvl, const yat::String& msg);
void exception_to_console(const yat::Exception& e);

//=============================================================================
//! Mix-in that add metadata capabilitie to objects
//=============================================================================
class MMetadata
{
public:
  enum Type
  {
    NONE = 0,
    STRING,
    INT,
    LONGINT,
    DOUBLE
  };

private:
  std::map<std::string, std::string> m_mapString; // "std::string" type metadata
  std::map<std::string, int> m_mapInteger;   // "Integer" type metadata
  std::map<std::string, long> m_mapLong;     // "Long Integer" type metadata
  std::map<std::string, double> m_mapDouble; // "Float" type metadata

public:
  //! Add std::string metadata
  //!
  //! \param strKey metadata name
  //! \param strValue metadata value
  void AddMetadata(const std::string &strKey, const std::string &strValue);

  //! Add C-ansi std::string metadata
  //!
  //! \param strKey metadata name
  //! \param pszValue metadata value
  void AddMetadata(const std::string &strKey, const char *pszValue);

  //! Add integer matedata
  //!
  //! \param strKey metadata name
  //! \param iValue metadata value
  void AddMetadata(const std::string &strKey, int iValue);

  //! Add integer matedata
  //!
  //! \param strKey metadata name
  //! \param lValue metadata value
  void AddMetadata(const std::string &strKey, long lValue);

  //! Add float metadata
  //!
  //! \param strKey metadata name
  //! \param dValue metadata value
  void AddMetadata(const std::string &strKey, double dValue);

  //! Check metadata
  //!
  //! \param strKey metadata name
  bool HasMetadata(const std::string &strKey) const;

  //! Get metadata as std::string
  //! Implicitely convert integer and float metadata as string value
  //!
  //! \param strKey metadata name
  //! \param[out] pstrValue Value as string
  //! \param  bThrow if true throw a exception in case of no data
  //! \return true if metadata was found, otherwise false (if bThrow == false)
  bool GetMetadata(const std::string &strKey, std::string *pstrValue, bool bThrow=true) const;

  //! Get string metadata
  //!
  //! \param strKey metadata name
  //! \param[out] pstrValue pointer output value
  //! \param  bThrow if true throw a exception in case of no data
  //! \return true if metadata was found, otherwise false (if bThrow == false)
  bool GetStringMetadata(const std::string &strKey, std::string *pstrValue, bool bThrow=true) const;

  //! Get string metadata
  //!
  //! \param strKey metadata name
  //! \return metadata value
  //! \throw Throw an exception if the key is not found
  std::string StringMetadata(const std::string &strKey) const;

  //! Get integer metadata
  //!
  //! \param strKey metadata name
  //! \param[out] piValue pointer output value
  //! \param  bThrow if true throw a exception in case of no data
  //! \return true if metadata was found, otherwise false (if bThrow == false)
  bool GetIntegerMetadata(const std::string &strKey, int *piValue, bool bThrow=true) const;

  //! Get integer metadata
  //!
  //! \param strKey metadata name
  //! \return metadata value
  //! \throw Throw an exception if the key is not found
  int IntegerMetadata(const std::string &strKey) const;

  //! Get long integer metadata
  //!
  //! \param strKey metadata name
  //! \param[out] plValue pointer output value
  //! \param  bThrow if true throw a exception in case of no data
  //! \return true if metadata was found, otherwise false (if bThrow == false)
  bool GetLongIntegerMetadata(const std::string &strKey, long *plValue, bool bThrow=true) const;

  //! Get long integer metadata
  //!
  //! \param strKey metadata name
  //! \return metadata value
  //! \throw Throw an exception if the key is not found
  long LongIntegerMetadata(const std::string &strKey) const;

  //! Get long float (double-type) metadata
  //!
  //! \param strKey metadata name
  //! \param[out] pdValue pointer output value
  //! \param  bThrow if true throw a exception in case of no data
  //! \return true if metadata was found, otherwise false (if bThrow == false)
  bool GetDoubleMetadata(const std::string &strKey, double *pdValue, bool bThrow=true) const;

  //! Get long float (double-type) metadata
  //!
  //! \param strKey metadata name
  //! \return metadata value
  //! \throw Throw an exception if the key is not found
  double DoubleMetadata(const std::string &strKey) const;

  //! Get type of a metadata from its key name
  //!
  //! \param strKey metadata name
  //! \param  bThrow if true throw a exception in case of no data
  //! \return MMetadata::Type value
  Type GetMetadataType(const std::string &strKey, bool bThrow=true) const;

  //! Returns the metadata keys std::list
  std::list<std::string> MetadataKeys() const;

  //! Clear all metadata
  void ClearMetadata();
};

//=============================================================================
//! Exception handling interface
//=============================================================================
class IExceptionHandler
{
public:
  //! Method to implement
  virtual void OnNexusException(const NexusException &e)=0;
};

//=============================================================================
//! Message handling interface
//=============================================================================
class IMessageHandler
{
public:
  //! Method to implement
  //!
  //! \param lvl log level as defined in the yat library
  //! \param msg the message
  virtual void OnNexusMessage(yat::ELogLevel lvl, const yat::String& msg)=0;
};

//=============================================================================
//! Synchronous/Asynchronous NeXus Writer File Class
//!
//! This class allow asynchronous writing into a nexus file
//! \remark This is wrapper class, the real job is make by a internal objet
//=============================================================================
class NEXUSCPP_DECL NexusFileWriter
{
public:
   enum WriteMode
  {
    SYNCHRONOUS = 0, //! Synchronous mode
    ASYNCHRONOUS,    //! Asynchronous mode

    ///@{ Depracated modes
    IMMEDIATE,
    DELAYED
    ///@}
  };

  //! Writing speed statistics
  typedef struct Statistics
  {
    Statistics();
    yat::uint64 ui64WrittenBytes;
    yat::uint64 ui64TotalBytes;
    float       fInstantMbPerSec; //! Instant rate in MBytes/s
    float       fAverageMbPerSec; //! Average rate in MBytes/s
    double      dTotalWriteTime;
  } Statistics;

  //=============================================================================
  //! Write notification interface
  //=============================================================================
  class INotify
  {
  public:
    //! Write subset notification
    //!
    //! \param[out] source_p object address who performed the write operation
    //! \param[out] dataset_path Path to the written dataset
    //! \param[out] start_pos start position of the subset
    //! \param[out] dim subset dimensions array
    virtual void OnWriteSubSet(NexusFileWriter* source_p, const std::string& dataset_path, int start_pos[MAX_RANK], int dim[MAX_RANK])=0;

    //! Write whole dataset notification
    //!
    //! \param[out] source_p object address who performed the write operation
    //! \param[out] dataset_path Path to the written dataset
    virtual void OnWrite(NexusFileWriter* source_p, const std::string& dataset_path)=0;

    //! File close notification
    //!
    //! \param[out] source_p object address who performed the write operation
    //! \param[out] nexus_file_path Closed file path
    virtual void OnCloseFile(NexusFileWriter* source_p, const std::string& nexus_file_path)=0;
  };

private:
  NexusFileWriterImpl *m_pImpl; // Implementation

  static std::size_t s_attempt_max_;        // max attempts for every nexus action
  static std::size_t s_attempt_delay_ms_;

public:
  //! Constructor
  //!
  //! \param strFilePath path + complete filename
  //! \param eMode Writing mode
  //! \param uiWritePeriod Write period (seconds) in asynchronous mode
  NexusFileWriter(const std::string &strFilePath, WriteMode eMode=ASYNCHRONOUS, unsigned int uiWritePeriod=2);

  //! Destructor
  ~NexusFileWriter();

  //! Set exception handler
  void SetExceptionHandler(IExceptionHandler *pHandler);

  //! Set message handler
  void SetMessageHandler(IMessageHandler *pHandler);

  //! Set the notification handler
  void SetNotificationHandler(INotify *pHandler);

  //! Set the notification handler
  void AddNotificationHandler(INotify* pHandler);

  //! Set cache size (in MB) for DELAYED mode
  void SetCacheSize(yat::uint16 usCacheSize);

  //! Create a dataset
  //!
  //! \param strPath  Dataset path and name (ex: "entry<NXentry>/sample<NXsample>/formula")
  //! \param eDataType The data type
  //! \param iRank number of dimensions
  //! \param piDim dimensions array
  //! \param filter Compression filter (no compression is the default)
  //! \param filter_config Compression filter parameters
  //! \param piChunk dimensions array of compress chunks (null if no compression)
  void CreateDataSet(const std::string &strPath, NexusDataType eDataType, int iRank,
                     int *piDim, CompressionFilter filter=CompressNone,
                     FilterConfig filter_config=FilterConfig(), int *piChunk=0);

  //! Push a dataset
  //!
  //! \param strPath  Dataset path and name (ex: "entry<NXentry>/sample<NXsample>/formula")
  //! \param ptrDataSet Shared pointer to the dataset to push
  void PushDataSet(const std::string &strPath, NexusDataSetPtr ptrDataSet);

  //! Push a attribute
  //! Dataset must already exists, otherwise a error is repported and action is canceled
  //!
  //! \param strPath Dataset path and name (ex: "entry<NXentry>/sample<NXsample>/formula.attribute")
  //! \param ptrAttr Shared pointer to the attribute
  void PushDatasetAttr(const std::string &strPath, NexusAttrPtr ptrAttr);

  //! Abort: delete all remaining data if any, no more access to file
  //! When this method returns
  void Abort();

  //! True
  bool IsError();

  //! Returns true if there is no data to write
  bool IsEmpty();

  //! Returns true when all job is done
  bool Done();

  //! Wait until all job is done
  void Synchronize(bool bSendSyncMsg=true);

  // Hold the data, force the NeXuisFileWriter to cache data and not writting it into the filename
  // Until either Synchronize() or Hold(false) is calles
  // Available only in asynchronous mode
  void Hold(bool b=true);

  // Return true if it hold the data
  // Make sense only in asynchronous mode
  bool IsHold() const;

  //! File name
  const std::string &File() const;

  //! The file will not be closed after each write action
  void SetFileAutoClose(bool b);

  //! Close the nexus file
  void CloseFile();

  //! Engage the file lock mecanism
  void SetUseLock();

  //@{ Attempt values for retry mechanism

    //! Max attempts for write actions
    static std::size_t AttemptMax() { return s_attempt_max_; }

    //! delay (in ms) between to attempts
    static std::size_t AttemptDelay() { return s_attempt_delay_ms_; }

    //! set max attempts for write actions
    static void SetAttemptMax(std::size_t n);

    //! set  (in ms) between to attempts
    static void SetAttemptDelay(std::size_t ms);

  //@}

  //@{ Statistics

    //! Reset all statistics
    void ResetStatistics();

    //! Gets a copy of the statistics
    Statistics GetStatistics() const;

  //@}
};

typedef yat::MemBuf DatasetBuf;

//! Referenced pointer definition
typedef yat::SharedPtr<NexusFileWriter, yat::Mutex> NexusFileWriterPtr;

//! Data shape type
typedef std::vector<std::size_t> DataShape;

// Empty shape
extern const DataShape g_empty_shape;

//==============================================================================
//! DatasetWriter
//!
//! class containing a NexusDataset with its attributes and its location
//==============================================================================
class NEXUSCPP_DECL DatasetWriter:public MMetadata
{
public:
  class IFlushNotification
  {
  public:
    virtual void OnFlushData(DatasetWriter* pWriter) = 0;
  };

  //! DatasetWriter configuration
  typedef struct Config
  {
    //! Dimensions array of canonical data
    DataShape shape_data_item;

    //! Dimensions array of acquisition space
    DataShape shape_matrix;

    //! Write cache size
    yat::uint16 cache_mb;

    //! useless parameter (to be deleted)
    yat::uint16 write_tmout;

    //! Compress filter
    CompressionFilter compress_filter;

    //! Compress filter parameters
    FilterConfig filter_config;

    //! minimal dataset size for compression, smaller datasets are not compressed
    std::size_t min_bytes_for_compression;

    //! default c-tor
    Config()
    {
      compress_filter = CompressNone;
      cache_mb = 64;
      write_tmout = 0;
      min_bytes_for_compression = 1024;
    }
  } Config;

private:
  Config              m_cfg;
  std::string         m_strPath;       // NeXus group path
  std::string         m_strDataset;    // Dataset name
  DatasetBuf          m_mbData;
  DataShape           m_shapeMatrix;   // shape of the dataitem matrix
  yat::uint16         m_usMaxMB;        // Max cached mega-bytes before flush
  NexusFileWriterPtr  m_ptrNeXusFileWriter;
  NexusDataType       m_NexusDataType;
  std::size_t         m_nDataItemRank;
  int                 m_aiDataItemDim[MAX_RANK];
  yat::uint32         m_ulDataItemSize;
  std::size_t         m_nTotalRank;
  int                 m_aiTotalDim[MAX_RANK];
  yat::uint64         m_ui64TotalSize;
  bool                m_bDatasetCreated;
  std::size_t         m_nDataItemCount;
  IFlushNotification* m_pFlushListener;
  bool                m_bUnlimited;         //#### true: Nexus dataset size is unlimited
  std::size_t         m_nWrittenCount;      // Count of written data items
  yat::Timeout        m_tmWriteTimeout;     // if expired, we should write the bufferized data

private:
  void CreateDataset();
  void Flush(bool call_on_flush);
  bool DirectFlush(const void *pData);
  void CheckWriter();
  void PreAllocBuffer();
  template <class TYPE>  void AdjustSizes();
  void init(yat::uint16 usMaxMB, yat::uint16 m_usWriteTimeout);

public:
  //! Sets the NeXus dataset type according to the class invoked through the template
  template <class TYPE>
  void SetDataType();

  //! reset the writer in order to re-using it
  void Reset();

  //! Sets the NeXus dataset type
  void SetNeXusDataType(NexusDataType eDataType);

  //! Set dataset shape
  //!
  //! \param shapeDataItem shape of the canonical data
  //! \param shapeMatrix shape of the acquisition
  void SetShapes(const DataShape &shapeDataItem, const DataShape &shapeMatrix);

  //! Set compression parameters
  void set_compression(CompressionFilter filter=CompressZLIB, FilterConfig filter_config=FilterConfig());

  //! Main constructor
  //!
  //! \param shapeDataItem Shape of the canonical data
  //! \param shapeMatrix Shape of the acquisition
  //! \param usMaxMB Cache size
  //! \param m_usWriteTimeout Max time before flushing cached data
  DatasetWriter(const DataShape &shapeDataItem, const DataShape &shapeMatrix=g_empty_shape, yat::uint16 usMaxMB=64, yat::uint16 m_usWriteTimeout=0);

  //! Convenience constructor for 1-D scan
  //!
  //! \param shapeDataItem Shape of the canonical data
  //! \param one_dim_size Scan length
  //! \param usMaxMB Cache size
  //! \param m_usWriteTimeout Max time before flushing cached data
  DatasetWriter(const DataShape &shapeDataItem, std::size_t one_dim_size, yat::uint16 usMaxMB=64, yat::uint16 m_usWriteTimeout=0);

  //! Use this constructor when shapes are now known yet
  //!
  //! \param usMaxMB Cache size
  //! \param m_usWriteTimeout Max time before flushing cached data
  DatasetWriter(yat::uint16 usMaxMB=64, yat::uint16 m_usWriteTimeout=0);
  DatasetWriter(const Config& cfg);

  //@{ Deprecated methods
    DatasetWriter(const std::vector<int>&, const std::vector<int>&, yat::uint16 usMaxMB=64);
    DatasetWriter(const std::vector<int>&, std::size_t, yat::uint16 usMaxMB=64);
  //@}

  /// d-tor
  virtual ~DatasetWriter();

  //! Set Nexus file writer object
  //! Must be called before push any data, like after construction or a call to the Stop method
  //!
  //! \param ptrWriter referenced pointer to the new writer object
  void SetNexusFileWriter(NexusFileWriterPtr ptrWriter);

  //! Set the flushing notification listener
  //! Must be called before push any data, like after construction or a call to the Stop method
  //!
  //! \param pListener pointer to the listener
  void SetFlushListener(IFlushNotification* pListener) { m_pFlushListener = pListener; }

  //! Resizes matrix
  //! Must be called before push any data, like after construction or a call to the Stop method
  //!
  //! \param shapeMatrix new shape
  void SetMatrix(const DataShape &shapeMatrix=g_empty_shape);

  //! Change destination path
  void SetPath(const std::string &strPath, const std::string &strDataset);

  //! Change destination path
  void SetFullPath(const std::string &strFullPath);

  //! Set the dataset name
  void SetDatasetName(const std::string &strDatasetName);

  //! Sets the buffer size in Mega bytes
  void SetCacheSize(yat::uint16 usMaxMB) { m_usMaxMB = usMaxMB; }

  //! Adding integer-type attribute to the NeXus dataset
  void AddAttribute(const NexusAttrPtr &ptrAttr);

  //! Adding double-type attribute to the NeXus dataset
  void AddFloatAttribute(const std::string &strName, double dValue);

  //! Adding integer-type attribute to the NeXus dataset
  void AddIntegerAttribute(const std::string &strName, long lValue);

  //! Adding std::string-type attribute to the NeXus dataset
  void AddStringAttribute(const std::string &strName, const std::string &strValue);

  //! Push data
  void PushData(const void *pData, std::size_t nDataCount=1, bool bNoCopy=false);

  //! No more data to push, data is flushed to the NeXus file writer
  void Stop();

  //! Cancel => forget all
  void Abort();

  //@{ Accessors
  const DataShape &MatrixShape() const { return m_shapeMatrix; }
  DataShape DataItemShape() const;
  NexusDataType DataType() const { return m_NexusDataType; }
  std::string FullPath() const;
  const std::string &DatasetName() const { return m_strDataset; }
  std::size_t TotalRank() const { return m_nTotalRank; }
  std::size_t DataItemCount() const { return m_nDataItemCount; }
  yat::uint64 TotalSize() const { return m_ui64TotalSize; }
  yat::uint32 DataItemSize() const { return m_ulDataItemSize; }
  yat::uint32 MaxDataItemsCount() const { return (unsigned long)(m_ui64TotalSize / m_ulDataItemSize); }
  //@}
};

//! Shared pointer definition
typedef yat::SharedPtr<DatasetWriter, yat::Mutex> DatasetWriterPtr;

//==============================================================================
//! GenericDatasetWriter
//!
//! Typed DatasetWriter  class
//==============================================================================
template <class TYPE>
class GenericDatasetWriter: public DatasetWriter
{
public:
  //! c-tor
  //! \param shapeDataItem Shape of the canonical data
  //! \param shapeMatrix Shape of the acquisition
  //! \param usMaxMB Cache size
  GenericDatasetWriter(const DataShape &shapeDataItem, const DataShape &shapeMatrix=g_empty_shape, unsigned short usMaxMB=100);

  //! d-tor
  ~GenericDatasetWriter() {}
};

//==============================================================================
//! AxisDatasetWriter
//!
//! Typed DatasetWriter class dedicated to for axis data
//==============================================================================
template <class TYPE>
class AxisDatasetWriter: public DatasetWriter
{
public:
  //! c-tor
  //! \param iDim Dimension number of axis
  //! \param iSize Dimension length
  //! \param iOrder Axis order in the dimension
  AxisDatasetWriter(int iDim, int iSize, int iOrder=1);

  //! d-tor
  ~AxisDatasetWriter() {}

  void PushPosition(TYPE TValue);
};

//==============================================================================
//! SignalDatasetWriter
//!
//! class containing a NexusDataset with its attributes and its location
//! Typed DatasetWriter class dedicated to for signal data
//==============================================================================
template <class TYPE>
class SignalDatasetWriter: public DatasetWriter
{
public:
  //! c-tor
  //! \param shapeData Shape of the canonical data
  //! \param shapeMatrix Shape of the acquisition
  //! \param iSignal axis order in the dimension
  SignalDatasetWriter(const DataShape &shapeData, const DataShape &shapeMatrix=g_empty_shape, int iSignal=1);

  //! d-tor
  ~SignalDatasetWriter() {}

  //! Push one position
  //!
  //! \param pValue pointer to the position value
  void PushSignal(TYPE *pValue);
};

#include <nexuscpp/impl/nxwriter.hpp>

} // namespace nxcpp


#endif

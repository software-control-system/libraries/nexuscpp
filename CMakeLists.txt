cmake_minimum_required(VERSION 3.15)
project (nexuscpp)

option(BUILD_SHARED_LIBS "Build using shared libraries" ON)

find_package(yat CONFIG REQUIRED)
find_package(HDF5 CONFIG REQUIRED)

add_compile_definitions(PROJECT_NAME=${PROJECT_NAME})
add_compile_definitions(PROJECT_VERSION=${VERSION})

add_compile_definitions(HDF_PROJECT_NAME=HDF5)
add_compile_definitions(HDF_PROJECT_VERSION=${HDF_PROJECT_VERSION})
add_compile_definitions(NEXUSCPP_HAVE_LZ4_FILTER)

if (UNIX)
  add_definitions(-std=c++0x)
  add_compile_definitions(Linux)
  add_compile_definitions(GNU_GCC)
  add_compile_definitions(_REENTRANT)
  add_compile_definitions(NEXUSCPP_HAVE_BSLZ4_FILTER)
endif()

if (WIN32)
  add_compile_definitions(_HDF5USEDLL_)
  add_compile_definitions(WITH_HDF5)
  if (BUILD_SHARED_LIBS MATCHES "ON")
    add_compile_definitions(NEXUSCPP_DLL)
    add_compile_definitions(NEXUSCPP_BUILD)
  endif()
endif()

add_subdirectory (src)

if (UNIX AND EXISTS ${CMAKE_SOURCE_DIR}/nexuscpp.pc.in)
    configure_file("${CMAKE_SOURCE_DIR}/nexuscpp.pc.in"
      "${CMAKE_BINARY_DIR}/nexuscpp.pc" @ONLY IMMEDIATE)
    install(FILES ${CMAKE_BINARY_DIR}/nexuscpp.pc DESTINATION lib/pkgconfig)
endif()
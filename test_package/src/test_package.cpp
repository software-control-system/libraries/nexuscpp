#include <iostream>
#include <nexuscpp/nexuscpp.h>

int main(int argc, char* argv[]) {
    std::cout << nxcpp::get_version() << std::endl;
    return 0;
}

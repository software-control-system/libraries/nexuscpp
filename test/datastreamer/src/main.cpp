//=============================================================================
//=============================================================================
//=============================================================================
//
// file :        main.cpp
//
//
// ********* Includes for test *********

#include <fcntl.h>
//#include <unistd.h>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <time.h>
#include <vector>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <typeinfo>

#include <yat/utils/String.h>
#include <yat/regex/Regex.h>
#include <yat/utils/Logging.h>
#include <yat/utils/CommandLine.h>
#include <yat/memory/MemBuf.h>
#include <yat/threading/Utilities.h>
#include <yat/file/FileName.h>
#include <yat/any/Any.h>
#include <yat/time/Time.h>
#include <yat/utils/Logging.h>
#include <yat/threading/Atomic.h>

#include "nexuscpp/nexuscpp.h"

namespace nx = nxcpp;

typedef yat::SharedPtr<nx::DataStreamer> DataStreamerPtr;
typedef yat::SharedPtr<nx::NexusDataStreamerFinalizer> NexusDataStreamerFinalizerPtr;

// ============================================================================
// Simulation config
// ============================================================================
class Simu : public nxcpp::IExceptionHandler, public nxcpp::IMessageHandler
{
public:
  static bool s_silent;

  struct DataItem
  {
    std::string name;
    std::vector<int> dimensions;
    std::string type_name;
    std::string fill_def;
    std::string fill_func;
    std::string fill_param;
    std::size_t data_size;
    std::size_t type_size;
    std::size_t push_count;      // Number of data items pushed at each push_data
    bool        no_copy;
    yat::Any last_value;
    yat::MemBuf data;
  };
  typedef yat::SharedPtr<DataItem> DataItemPtr;

  struct Stream
  {
    std::string name;
    std::vector<DataItemPtr> items;
  };

  struct Config
  {
    bool infinite_mode;
    bool use_finalizer;
    bool asynchronous_write;
    std::size_t step_count;
    std::size_t buffer_size;
    std::size_t nb_acq;
    yat::int32 sleep_time_ms;
    std::string dest_path;
    std::vector<Stream> streams;
    std::size_t compress_level;
    yat::String compress_filter;
  };

  void read(const std::string& config_file);
  void read_data_item(yat::CfgFile& cfg_file, Simu::DataItem& item);
  void fill_data_item(Simu::DataItem& item, bool alloc_memory);
  void read_stream_params(yat::CfgFile& cfg_file, Simu::Stream& stream);
  void from_file(const yat::String& file_name, Simu::DataItem& item);

  const Config& config() { return m_config; }

  Simu(const std::string& config_file);
  void run_acq(NexusDataStreamerFinalizerPtr finalizer_ptr);
  void start();
  void dump();
  void free_ds(std::map<std::string, nx::DataStreamer*>& stream_map);

  virtual void OnNexusException(const nxcpp::NexusException &e)
  {
    YAT_LOG_EXCEPTION(e);
    m_error_flag = true;
  }
  virtual void OnNexusMessage(yat::ELogLevel lvl, const yat::String& msg)
  {
    yat::fcout("{}").arg(msg);
  }

private:
    Config m_config;
    yat::atomic_bool   m_error_flag;
};

bool Simu::s_silent = false;

//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
template<typename T>
static std::string VectorToStr( const std::vector<T> &v, char left='[', char sep=',', char right=']' )
{
  std::ostringstream oss;
  oss << left;
  typename std::vector<T>::const_iterator it;
  it = v.begin();

  while( it != v.end() )
  {
    oss << *it;
    ++it;
    if( it != v.end() )
      oss << sep;
  }
  oss << right;
  return oss.str();
}

//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
Simu::Simu(const std::string& config_file)
{
  std::cout << "Simu @" << this << std::endl;
  m_error_flag = false;
  m_config.infinite_mode = false;
  m_config.buffer_size = 0;
  m_config.sleep_time_ms = 0;
  m_config.nb_acq = 1;
  m_config.use_finalizer = false;
  m_config.asynchronous_write = false;
  read(config_file);
}

#define OUT_LN(s) if( !Simu::s_silent) { std::clog << s << std::endl; }
#define OUT(s) if( !Simu::s_silent) { std::clog << s; }

#define TO_INTEGER(s) atol(PSZ(s))

#define READ_INTEGER_VALUE(f, obj, name) \
  obj.name = TO_INTEGER(f.get_param_value(#name))

#define READ_OPT_INTEGER_VALUE(f, obj, name, default) \
  if( f.has_parameter(#name) ) \
    { obj.name = TO_INTEGER(f.get_param_value(#name)); } \
  else \
    { obj.name = default; }

#define READ_STRING_VALUE(f, obj, name ) \
  obj.name = f.get_param_value(#name)

#define READ_INTEGER_VECTOR(f, obj, name, sep) \
  std::string s = f.get_param_value(#name); \
  std::vector<std::string> v; \
  yat::StringUtil::split(&s, sep, &v); \
  for(size_t i = 0; i < v.size(); ++i ) \
    obj.name.push_back(atol(PSZ(v[i])))

#define STRING_TO_STRING_VECTOR(f, name, sep) \
  std::vector<std::string> name; \
  std::string s = f.get_param_value(#name); \
  yat::StringUtil::split(&s, sep, &name);

#define READ_BOOLEAN_VALUE(f, obj, name) \
  { \
  std::string n##_s = f.get_param_value(#name); \
    if( yat::StringUtil::is_equal_no_case(n##_s, "true") \
      || yat::StringUtil::is_equal(n##_s, "1") ) \
    obj.name = true; \
  else \
    obj.name = false;\
  }

#define READ_OPT_BOOLEAN_VALUE(f, obj, name, default) \
  if( f.has_parameter(#name) ) \
  { \
    std::string n##_s = f.get_param_value(#name); \
    if( yat::StringUtil::is_equal_no_case(n##_s, "true") \
      || yat::StringUtil::is_equal(n##_s, "1") ) \
      obj.name = true; \
    else \
      obj.name = false; \
  } \
  else \
    { obj.name = default; }

#define ANY_T_VALUE(any, item_T_name, T_name, T, s_value) \
  { \
    std::istringstream iss(s_value); \
    if( item_T_name == std::string(T_name) ) \
    { \
      T value; \
      iss >> value; \
      any = value; \
    }\
  }

#define T_ALLOC_MEM(item, T_name, T) \
  { \
    if( item_T_name == std::string(T_name) ) \
    { \
      item.type_size = sizeof(T); \
      membuf.set_len(count * size * sizeof(T)); \
    } \
  }

#define T_FILL_ITEM_CONSTANT(item, T_name, T) \
  { \
    std::istringstream iss(s_value); \
    if( item_T_name == std::string(T_name) ) \
    { \
      T value; \
      iss >> value; \
      membuf.set_len(0); \
      for( std::size_t i = 0; i < count * size; ++i ) \
        membuf.put_bloc(&value, sizeof(T)); \
    } \
  }

#define T_FILL_ITEM_LINEAR(item, T_name, T) \
  { \
    std::istringstream iss(s_offset); \
    if( item_T_name == std::string(T_name) ) \
    { \
      T offset; \
      iss >> offset; \
      T value = offset; \
      if( !item.last_value.empty() ) \
        value = yat::any_cast<T>(item.last_value); \
      membuf.set_len(0); \
      for( std::size_t i = 0; i < count * size; ++i ) \
      { \
        membuf.put_bloc(&value, sizeof(T)); \
        value += T(1); \
      } \
      item.last_value = value; \
    } \
  }

#define T_FILL_ITEM_SINUS(item, T_name, T) \
  { \
    if( item_T_name == std::string(T_name) ) \
    { \
      std::vector<std::string> params; \
      yat::StringUtil::split(s_params, ',', &params); \
      std::istringstream issCoeff(params[0]); \
      std::istringstream issPeriod(params[1]); \
      std::istringstream issDelta(params[2]); \
      T coeff, period, value, Delta, idx = 0; \
      if( !item.last_value.empty() ) \
        idx = yat::any_cast<T>(item.last_value); \
      double pi = 3.14159265; \
      issCoeff >> coeff; \
      issPeriod >> period; \
      issDelta >> Delta; \
      membuf.set_len(0); \
      for( std::size_t i = 0; i < count * size; ++i ) \
      { \
        value = coeff * sin(((idx + i) * 360 / period) * pi / 180); \
        membuf.put_bloc(&value, sizeof(T)); \
      } \
      item.last_value = T(idx + Delta); \
    } \
  }

#define PUSH_DATA(s_ptr, membuf, item_name, item_T_name, T_name, T, count) \
  if( item_T_name == std::string(T_name) ) \
  { \
    s_ptr->PushData<T>(item_name, (T *)(membuf.buf()), count); \
  }

//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
void Simu::dump()
{
  OUT_LN("Simulation");
  OUT_LN("infinite_mode    : " << m_config.infinite_mode);
  OUT_LN("step_count       : " << m_config.step_count);
  OUT_LN("buffer_size      : " << m_config.buffer_size);
  OUT_LN("sleep_time_ms    : " << m_config.sleep_time_ms);
  OUT_LN("dest_path        : " << m_config.dest_path);

  for( std::size_t i = 0; i < m_config.streams.size(); ++i )
  {
    OUT_LN("Stream           : " << m_config.streams[i].name);
    for( std::size_t j = 0; j < m_config.streams[i].items.size(); ++j )
    {
      OUT_LN("  Item           : " << m_config.streams[i].items[j]->name);
      OUT_LN("    type         : " << m_config.streams[i].items[j]->type_name);
      OUT_LN("    dimensions   : " << VectorToStr<int>(m_config.streams[i].items[j]->dimensions));
      OUT_LN("    fill         : " << m_config.streams[i].items[j]->fill_def);
      OUT_LN("    push count   : " << m_config.streams[i].items[j]->push_count);
    }
  }
}

//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
void Simu::fill_data_item(Simu::DataItem& item, bool alloc_memory)
{
  std::string func_part = item.fill_func;
  std::string fill_def = item.fill_param;

  std::string s_value = item.fill_param;
  std::string item_T_name = item.type_name;
  std::string s_offset = item.fill_param;
  std::string s_params = item.fill_param;
  yat::MemBuf& membuf = item.data;
  std::size_t count = item.push_count;
  std::size_t size = item.data_size;

  if( alloc_memory )
  {
    T_ALLOC_MEM(item, "short", short);
    T_ALLOC_MEM(item, "unsigned short", unsigned short);
    T_ALLOC_MEM(item, "long", long);
    T_ALLOC_MEM(item, "unsigned long", unsigned long);
    T_ALLOC_MEM(item, "float", float);
    T_ALLOC_MEM(item, "double", double);
  }

  if( yat::StringUtil::is_equal_no_case(func_part, "const") || yat::StringUtil::is_equal_no_case(func_part, "constant") )
  {
    T_FILL_ITEM_CONSTANT(item, "short", short);
    T_FILL_ITEM_CONSTANT(item, "long", long);
    T_FILL_ITEM_CONSTANT(item, "float", float);
    T_FILL_ITEM_CONSTANT(item, "double", double);
  }
  else if( yat::StringUtil::is_equal_no_case(func_part, "lin") || yat::StringUtil::is_equal_no_case(func_part, "linear") )
  {
    T_FILL_ITEM_LINEAR(item, "short", short);
    T_FILL_ITEM_LINEAR(item, "long", long);
    T_FILL_ITEM_LINEAR(item, "float", float);
    T_FILL_ITEM_LINEAR(item, "double", double);
  }
  else if( yat::StringUtil::is_equal_no_case(func_part, "sin") || yat::StringUtil::is_equal_no_case(func_part, "sinus") )
  {
    T_FILL_ITEM_SINUS(item, "short", short);
    T_FILL_ITEM_SINUS(item, "long", long);
    T_FILL_ITEM_SINUS(item, "float", float);
    T_FILL_ITEM_SINUS(item, "double", double);
  }
#ifdef YAT_LINUX
  else if( yat::StringUtil::is_equal_no_case(func_part, "file") )
  {
    if( alloc_memory )
    {
      int fi = open(fill_def.c_str(), O_RDONLY);
      ssize_t r = ::read(fi, membuf.buf(), item.data_size * item.type_size);
      if( r != item.data_size * item.type_size )
      {
        std::cout << "Can't read file " << fill_def << std::endl;
        exit(1);
      }
      close(fi);
    }
  }
#endif
  else if( yat::StringUtil::is_equal_no_case(func_part, "no_value") )
  {
    // nothing to do
  }
  else
    throw yat::Exception("ERR_CONFIG", "bad item fill definition", "Simu::read_data_item");
}

//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
void Simu::from_file(const yat::String& file_name, Simu::DataItem& item)
{
  yat::FileName fn(file_name);
  if( !fn.file_exist() )
    return;

  struct stat st;
  if( stat(fn.full_name().c_str(), &st) )
    return;

  std::size_t size = st.st_size;
  std::size_t count = 1;

  yat::String name = fn.name();
  yat::Regex re("([_[:alnum:]]+)-([[:digit:]]+)x?([[:digit:]]+)?_([[:alnum:]]+)");
  yat::Regex::Match m;
  re.match(name, &m);
  if( m.empty() )
    return;

  int size_1 = m.str(2).to_num<int>();
  int size_2 = m.str(3).empty() ? 0 : m.str(3).to_num<int>();
  yat::String type   = m.str(4);

  item.dimensions.push_back(size_1);
  if( size_2 > 0 )
    item.dimensions.push_back(size_2);

  if( type.is_equal_no_case("int16") || type.is_equal_no_case("short") )
    item.type_name = "short";
  if( type.is_equal_no_case("uint16") || type.is_equal_no_case("ushort") )
    item.type_name = "unsigned short";
  if( type.is_equal_no_case("int32") || type.is_equal_no_case("long") )
    item.type_name = "long";
  if( type.is_equal_no_case("uint32") || type.is_equal_no_case("ulong") )
    item.type_name = "unsigned long";
  if( type.is_equal_no_case("float32") || type.is_equal_no_case("float") )
    item.type_name = "float";
  if( type.is_equal_no_case("float64") || type.is_equal_no_case("double") )
    item.type_name = "double";

  if( item.type_name.empty() )
    return;

  item.data_size = size;
  item.push_count = 1;
  item.fill_func = "file";
  item.fill_param = fn.full_name();
}

//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
void Simu::read_data_item(yat::CfgFile& cfg_file, Simu::DataItem& item)
{
  cfg_file.set_section(item.name);
  if( cfg_file.has_parameter("from_file") )
    from_file(cfg_file.get_param_value("from_file"), item);
  else
  {
    READ_INTEGER_VECTOR(cfg_file, item, dimensions, ',');
    READ_STRING_VALUE(cfg_file, item, type_name);
    READ_OPT_INTEGER_VALUE(cfg_file, item, push_count, 1);
    READ_BOOLEAN_VALUE(cfg_file, item, no_copy);

    item.fill_def = cfg_file.get_param_value("fill");
    std::string fill_def = item.fill_def;
    std::string func_part;
    yat::StringUtil::extract_token(&fill_def, ',', &func_part);
    yat::StringUtil::trim(&func_part);
    yat::StringUtil::trim(&fill_def);
    item.fill_func = func_part;
    item.fill_param = fill_def;
  }

  item.data_size = 1;
  for( std::size_t dim = 0; dim < item.dimensions.size(); ++dim )
    item.data_size *= item.dimensions[dim];
std::cout << "size " << item.data_size << std::endl;
}

//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
void Simu::read_stream_params(yat::CfgFile& cfg_file, Simu::Stream& stream)
{
  cfg_file.set_section(stream.name);
  std::vector<std::string> items;
  std::string val = cfg_file.get_param_value("items");
  yat::StringUtil::split(&val, ',', &items);

  for( std::size_t i=0; i < items.size(); ++i )
  {
    DataItemPtr item_ptr(new DataItem);
    std::string item_name = items[i];
    yat::StringUtil::trim(&item_name);
    (*item_ptr).name = item_name;
    read_data_item(cfg_file, *item_ptr);
    stream.items.push_back(item_ptr);
  }
}

//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
void Simu::read(const std::string& file_name)
{
  yat::CfgFile cfg_file(file_name);
  cfg_file.load();

  cfg_file.set_section("global");
  if( cfg_file.has_parameter("silent") )
  {
    std::string silent = cfg_file.get_param_value("silent");
    if( yat::StringUtil::is_equal_no_case(silent, "true")
     || yat::StringUtil::is_equal(silent, "1") )
      Simu::s_silent = true;
  }

  cfg_file.set_section("acquisition");
  READ_BOOLEAN_VALUE(cfg_file, m_config, infinite_mode);
  READ_BOOLEAN_VALUE(cfg_file, m_config, use_finalizer);
  READ_BOOLEAN_VALUE(cfg_file, m_config, asynchronous_write);
  READ_OPT_INTEGER_VALUE(cfg_file, m_config, compress_level, 0);
  READ_STRING_VALUE(cfg_file,  m_config, compress_filter);
  READ_INTEGER_VALUE(cfg_file, m_config, buffer_size);
  READ_INTEGER_VALUE(cfg_file, m_config, step_count);
  READ_INTEGER_VALUE(cfg_file, m_config, sleep_time_ms);
  READ_INTEGER_VALUE(cfg_file, m_config, nb_acq);
  READ_STRING_VALUE(cfg_file, m_config, dest_path);
  STRING_TO_STRING_VECTOR(cfg_file, streams, ',');

  for( std::size_t i=0; i < streams.size(); ++i )
  {
    Simu::Stream stream;
    std::string name = streams[i];
    yat::StringUtil::trim(&name);
    stream.name = name;
    read_stream_params(cfg_file, stream);
    m_config.streams.push_back(stream);
  }
}

//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
void Simu::free_ds(std::map<std::string, nx::DataStreamer*>& stream_map)
{
    for( std::map<std::string, nx::DataStreamer*>::iterator it = stream_map.begin();
         it != stream_map.end(); ++it )
    {
      if( it->second )
        delete it->second;
    }
}

//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
void Simu::run_acq(NexusDataStreamerFinalizerPtr finalizer_ptr)
{
  nx::DataStreamer::Statistics stats;
  // Prepare the streams
  std::map<std::string, nx::DataStreamer*> stream_map;
  for( std::size_t i = 0; i < m_config.streams.size(); ++i )
  {
    Stream& stream = m_config.streams[i];
    nx::DataStreamer::Config cfg;

    if( !m_config.infinite_mode )
      cfg.acquisition_size = m_config.step_count;

    cfg.buffer_name = stream.name;
    cfg.buffer_size = m_config.buffer_size;
    cfg.target_path = m_config.dest_path;
    cfg.write_mode = m_config.asynchronous_write ? nx::NexusFileWriter::ASYNCHRONOUS : nx::NexusFileWriter::SYNCHRONOUS;
    cfg.max_attempts = 10;
    cfg.retry_delay = 200;

    if( m_config.compress_level > 0 )
      cfg.filter_config[nxcpp::compression_level] = m_config.compress_level;
    if( m_config.compress_filter.is_equal_no_case("zlib") )
      cfg.compress_filter = nxcpp::CompressZLIB;
    if( m_config.compress_filter.is_equal_no_case("lz4") )
      cfg.compress_filter = nxcpp::CompressLZ4;
    if( m_config.compress_filter.is_equal_no_case("bslz4") )
    {
      cfg.compress_filter = nxcpp::CompressBsLZ4;
    }

    cfg.exception_handler_p = this;
    cfg.message_handler_p = this;
    OUT_LN("compress level: " << m_config.compress_level);

//    cfg.throw_if_file_exists = true;

    nx::DataStreamer* streamer_p = new nx::DataStreamer(cfg);

    stream_map[stream.name] = streamer_p;

    for( std::size_t j = 0; j < stream.items.size(); ++j )
    {
      DataItem& item = *(stream.items[j]);
      streamer_p->AddDataItem(item.name, item.dimensions);
      if( item.no_copy )
      {
        streamer_p->SetDataItemMemoryMode(item.name, nx::DataStreamer::NO_COPY);
        OUT_LN("No copy for " << item.name);
      }
    }
  }

  try
  {
    std::cout << std::unitbuf << '0';
    // Loop over simulated scan data
    int progress = 0, last_progress10 = 0, last_progress = 0;
    for( std::size_t i = 0; i < m_config.step_count && !m_error_flag; ++i )
    {
      // progress info
      progress = (i * 100 ) / m_config.step_count;
      if( progress - last_progress10 >= 10 && m_config.step_count >= 20 )
      {
        for( last_progress; last_progress < progress - 1; ++last_progress )
          std::cout << std::unitbuf << '.';
        std::cout << std::unitbuf << progress;
        last_progress10 = progress;
        last_progress = progress;
      }
      else if( progress - last_progress >= 1 )
      {
        for( last_progress; last_progress < progress; ++last_progress )
          std::cout << std::unitbuf << '.';
      }

      // data streaming
      for( std::size_t j = 0; j < m_config.streams.size(); ++j )
      {
        Stream& stream = m_config.streams[j];
        for( std::size_t k = 0; k < stream.items.size(); ++k )
        {
          DataItem& item = *(stream.items[k]);

          if( i % item.push_count == 0 )
          { // Push data every push_count steps
            fill_data_item(item, i == 0 ? true : false);
            PUSH_DATA(stream_map[stream.name], item.data, item.name, item.type_name, "short", short, item.push_count);
            PUSH_DATA(stream_map[stream.name], item.data, item.name, item.type_name, "unsigned short", unsigned short, item.push_count);
            PUSH_DATA(stream_map[stream.name], item.data, item.name, item.type_name, "long", long, item.push_count);
            PUSH_DATA(stream_map[stream.name], item.data, item.name, item.type_name, "unsigned long", unsigned long, item.push_count);
            PUSH_DATA(stream_map[stream.name], item.data, item.name, item.type_name, "float", float, item.push_count);
            PUSH_DATA(stream_map[stream.name], item.data, item.name, item.type_name, "double", double, item.push_count);
          }
        }
      }

      if( m_config.sleep_time_ms > 0 )
      {
        yat::ThreadingUtilities::sleep(m_config.sleep_time_ms / 1000,
                                       (m_config.sleep_time_ms % 1000) * 1000000);
      }

    }
    for( last_progress; last_progress < 99; ++last_progress )
      OUT(std::unitbuf << '.');

    // Done

    for( std::map<std::string, nx::DataStreamer*>::iterator it = stream_map.begin();
         it != stream_map.end(); ++it )
    {
      if( finalizer_ptr )
      {
        nx::NexusDataStreamerFinalizer::Entry *e = new nx::NexusDataStreamerFinalizer::Entry();
        e->data_streamer = it->second;
        finalizer_ptr->push(e);
      }
      else
      {
        if( m_config.infinite_mode )
          it->second->Stop();
        it->second->Finalize();
        delete it->second;
        it->second = 0;
      }
    }
    OUT_LN("100\n");
  }
  catch(nx::NexusException &ne)
  {
    LOG_EXCEPTION("nexus", ne);
    ne.dump();
    free_ds(stream_map);
    exit(0);
  }
  catch(yat::Exception& ex)
  {
    YAT_LOG_EXCEPTION(ex);
    ex.dump();
    free_ds(stream_map);
    exit(0);
  }
  catch(...)
  {
    free_ds(stream_map);
    exit(0);
  }
}

//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
void Simu::start()
{
  NexusDataStreamerFinalizerPtr finalizer_ptr;
  if( m_config.use_finalizer )
  {
    finalizer_ptr = new nx::NexusDataStreamerFinalizer;
    finalizer_ptr->start();
  }
  for( std::size_t i = 0; i < m_config.nb_acq; ++i )
  {
    yat::log_info(yat::StringFormat("acq #{}").format(i));
    run_acq(finalizer_ptr);
  }

  if( finalizer_ptr )
  {
    OUT_LN("Terminate finalizer...");
    finalizer_ptr->stop();
    yat::ThreadingUtilities::sleep(3);
  }

  OUT_LN("Done.");
}

//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
int main(int argc,char *argv[])
{
  try
  {
    yat::CommandLine cl;
    cl.add_opt('c', "config", "file", "config file name");
    cl.read(argc, argv);

    if( cl.is_option("config") )
    {
      Simu simu(cl.option_value("config"));
      simu.dump();

      yat::fcout("Compression filters availability");
      yat::fcout("{.<11}: {}").arg("ZLIB").arg(nxcpp::IsCompressFilterAvailable(nxcpp::CompressZLIB));
      yat::fcout("{.<11}: {}").arg("LZ4").arg(nxcpp::IsCompressFilterAvailable(nxcpp::CompressLZ4));
      yat::fcout("{.<11}: {}").arg("BitShuffle").arg(nxcpp::IsCompressFilterAvailable(nxcpp::CompressBsLZ4));
      yat::fcout("{.<11}: {}").arg("Zstd").arg(nxcpp::IsCompressFilterAvailable(nxcpp::CompressZstd));
      yat::fcout("{.<11}: {}").arg("Blosc2").arg(nxcpp::IsCompressFilterAvailable(nxcpp::CompressBlosc2));
      yat::fcout("{.<11}: {}").arg("Bzip2").arg(nxcpp::IsCompressFilterAvailable(nxcpp::CompressBzip2));

      yat::CurrentTime tm_beg;
      simu.start();
      yat::CurrentTime tm_end;
      yat::fcout("Elapsed time: {}").arg((tm_end.raw_value() - tm_beg.raw_value()) / 1000000.0);

    }
    else
      cl.show_usage("missing option --config"); //throw yat::Exception("ERR_CONFIG", "missing option --config", "main");
  }
  catch(nx::NexusException &ne)
  {
    LOG_EXCEPTION("nexus", ne);
  }
  catch(yat::Exception& ex)
  {
    ex.dump();
  }
  catch(std::exception &e)
  {
    std::cout<<"exception catchée 4"<<std::endl;
    std::cout<<e.what()<<std::endl;
  }

  return(0);
}
